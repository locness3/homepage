const navigationPlugin = require("@11ty/eleventy-navigation");

module.exports = function(eleventyConfig) {
	eleventyConfig.addPlugin(navigationPlugin);
	eleventyConfig.setTemplateFormats([
		"md", "liquid",
		"css", "png" // https://www.11ty.dev/docs/copy/#passthrough-by-file-extension
	]);
return {
	dir: {
		includes: "_includes",
		layouts: "_layouts"
	}
}
};
