[main page](/)

last updated: see [update history on git](https://codeberg.org/locness3/homepage/commits/branch/master/roadmap.md)

# planned things for this server


for when i can give myself enough free time

-   ~~have some form of (automated or not) backup~~
    -   this thing\'s been running on the same sd card for 3+ years. at
        time of writing no backup exists.
-   get rid of the different shaarlis to only have one;
    -   any alternative to keeping them online for permalink posterity?
-   ~~clean up dokuwiki; archive its content as static pages~~;
-   ~~get rid of useless/stupid content in the files directory and
    elsewhere~~;
-   get rid of unused private services;
-   update to newer Raspbian;
-   switch from nginx to something else;
    -   need to get familiar with the alternatives
-   switch from duckdns to something else
    -   because duckdns is hosted by AWS, and requires login via a \"big
        tech\" service
    -   current domain will be temporarily kept alive as redirect
    -   probably freedns.afraid.org

# might do

-   a less ugly homepage;
-   consistency between different areas;
-   bring back a cors proxy (that does not have access to my home
    network);
-   a weblog;
-   more private services for me and people i know;
-   switch to another distro (in place of or after upgrading Raspbian);
	- initially thought of Alpine but [installation process on Raspberry Pi looks confusing at first glance](https://wiki.alpinelinux.org/wiki/Raspberry_Pi)
-   kill the whole self-hosted thing and switch to some hosting
    service/shared system;
-   touch grass
