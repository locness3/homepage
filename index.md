---
layout: default.liquid
---

# \<big>\<marquee>welcome to the locness-space\</marquee>

## Under construction

I am currently in the process of cleaning up and improving the space, and I laid down a [roadmap](roadmap)

In particular, I have started to move content currently scattered accross various apps on this space (Shaarli, DokuWiki) to a static website built from Markdown files. I will then work on making it more complete and organized. [Source files are here](https://codeberg.org/locness3/homepage) and are currently made into a website via [Eleventy](https://11ty.dev), but I might try other tools.

I have deleted and am still deleting some old content. If you need anything, try your luck at the [Wayback Machine](https://web.archive.org) or contact me.

## On the space
<nav class="index-nav">

- [Links](links)
- [Resources](res)

</nav>

## About me

My most used username is locness3, so my personal web-space is the locness-space.

You can contact me via locness3 at e dot email.


[Old main page](index.old.html)

