---
title: "Awesome FOSS apps for Android"
---
# \[NOT DONE\] List of awesome free and open-source Android apps

## Utilities

-   [NetGuard](https://f-droid.org/en/packages/eu.faircode.netguard/) :
    I wish I knew about this earlier. Allows restricting Internet access
    per application\.... and also works with system apps, that includes
    Google and OEM spyware. Without any root! It can also block unwanted
    requests via a hosts file.
-   [Calculator++](https://f-droid.org/fr/packages/org.solovyev.android.calculator/)
    : A scientific calculator with a nice interface and a lot of
    features.
-   [QR & Barcode
    Scanner](https://f-droid.org/fr/packages/com.example.barcodescanner/)
    : A beautifully-designed app that does a little more than what its
    name says.
-   [QuickTiles](https://f-droid.org/fr/packages/com.asdoi.quicktiles/)
    : A collection of many useful settings tiles.

## Multimedia

-   [mpv](https://f-droid.org/en/packages/is.xyz.mpv/) : The small media
    player that does many things.
-   [Shuttle+](https://f-droid.org/fr/packages/com.simplecity.amp_pro/)
    : A complete local music player with a polished interface

## Internet and messaging

-   [Midori](https://f-droid.org/en/packages/org.midorinext.android/) :
    The underrated web browser. Lightweight (uses system webview) with
    many useful features and customizations.
-   [Fennec](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/)
    : Build of Mozilla\'s [Firefox browser for
    Android](https://www.mozilla.org/en-US/firefox/mobile/). Features
    \"tracking protection\" and support for add-ons.
-   [Conversations](https://f-droid.org/en/packages/eu.siacs.conversations/)
    : a modern [XMPP](https://xmpp.org/about/technology-overview.html)
    client, with support for
    [MUC](https://xmpp.org/about/technology-overview.html#muc),
    encryption, audio and video calls\... Makes a decent messaging
    experience under your control.

## \"Productivity\"

## Gameing™

## Customization

## Various
