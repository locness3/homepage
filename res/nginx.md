---
title: Notes about nginx
---

# Notes about nginx

of course, `location ^~ /l` will match `/l/stuff` as well as
`/logos/stuff`. adding a trailing slash in the location pattern seems to
fix it.

## Single location block and priority

*Originally [a thread](https://librosphere.fr/notice/AJRogM3lxwD8pTLAZM)
on fediverse*

Nginx only uses [a single location
block](https://serverfault.com/questions/696396/nginx-location-block-only-match-actual-files)
when processing requests. If multiple location blocks correspond to a
request, nginx will
[prioritise](https://stackoverflow.com/questions/5238377/nginx-location-priority)
one.

## Directive inheritance

<https://stackoverflow.com/questions/32104731/directive-inheritance-in-nested-location-blocks>

FIXME : add personal example

## Bug with try_files

FIXME document this

https://trac.nginx.org/nginx/ticket/97

## URI passed to proxied server

FIXME
<https://stackoverflow.com/questions/58911675/how-to-forward-all-paths-that-start-with-a-specific-location-in-nginx>

## An alternative to try_files
article [try_files is evil too](https://www.getpagespeed.com/server-setup/nginx-try_files-is-evil-too)
considers try_files to be a performance penalty because of unnecessary file existence checks
and describes how not to use it - interesting.
