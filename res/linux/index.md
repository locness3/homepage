---
title: Linux
---

# Linux

## Distributions

### Raspberry Pi

-   [Raspberry Pi OS](raspbian)

## Desktop Environments

-   [Locking down desktop environments](desktop-lockdown)

## Community and culture

### sudo rm -rf \--no-preserve-root / (and all its variations)

A command like this will erase anything present on the computer and all
connected(only mounted?) storage:

-   `sudo`: get root privileges, aka full control over the computer
-   `rm` = **r**e**m**ove
-   `-` begins a series of flags
    -   `r` = **r**ecursive: remove directories
    -   `f` = **f**orcefully remove files
-   `--no-preserve-root` is an additional flag added specifically for
    confirming this type of command
-   `/` means the root of the filesystem, containing all system files,
    personal files in /home, and mount points for other storage

Some people find it funny to tell newbies to run this kind of command
when they\'re asking for help and have no idea of what it does. FIXME :
add examples It fucking isn\'t.
