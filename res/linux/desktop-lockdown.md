---
title: "Locking down Linux desktop environments"
parent: Linux
---

# Locking down Linux desktops

The ability to lock down desktop environments on Linux is something that
really intersts me, because I think Linux is really suited to be
installed on publicly accessible computers, like in librairies or
schools. In these contexts, I think it\'s better to lock down the
customization possibilities of the desktop. FIXME why?

I have been playing around with various desktop environments, trying to
lock them down for use in a hypothetical public computer scenario.

## Cinnamon

I thought I\'d start with
[Cinnamon](https://projects.linuxmint.com/cinnamon/) first, because
it\'s modern and familiar, yet lightweight.

I did not find any administration guide specifically for Cinnamon.

### Locking down \"core\" settings via dconf

Cinnamon was forked from GNOME at the start. Like most GNOME-related
projects, it uses [GSettings](https://wiki.gnome.org/HowDoI/GSettings)
for managing its \"core\" settings, which in turn uses
[dconf](https://wiki.gnome.org/Projects/dconf)
([Wikipedia](https://en.wikipedia.org/wiki/Dconf)) as a backend (in most
setups).

(The next section will explain why I\'m refering to \"core\" settings)

dconf allows locking down settings as detailed in [the dconf System
Administrator
Guide](https://wiki.gnome.org/Projects/dconf/SystemAdministrators) and
the [GNOME System Administration
Guide](https://help.gnome.org/admin/system-admin-guide/stable/dconf-lockdown.html.en).
This last guide also provides guidance to locking down many useful
settings and features.

Many settings in Cinnamon are very similar to GNOME, except that
they\'re stored in /org/cinnamon rather than /org/gnome.

(I\'d still recommend looking around in dconf-editor to find available
settings and their expected values rather than blindly replacing
/org/gnome with /org/cinnamon in GNOME\'s guide.)

### About applet settings

FIXME

not applet settings :
<https://github.com/linuxmint/cinnamon/issues/2719#issuecomment-1204127323>
ref <https://github.com/linuxmint/cinnamon/issues/4720>

### hiding setting menus?

FIXME

hide settings panels from menu by editing desktop file :
<https://wiki.archlinux.org/title/Desktop_entries>

haven\'t found how to hide from system settings, might just hide that
app

### Other things

FIXME seemingly can\'t disable looking glass (cinnamopn\'s debugger)
