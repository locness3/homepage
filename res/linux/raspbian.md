---
title: About Raspberry Pi OS
parent: Linux
---

# Raspberry Pi OS

This new name is long and obscure, I much prefer the older name :

# Raspbian

I\'ll be calling it it this way throughout the rest of this page, do not
be surprised.

## Notable changes in Bullseye

FIXME
<https://www.raspberrypi.com/news/raspberry-pi-os-debian-bullseye/>
<https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/>

## Using another desktop environment

### Login screen

* [Discord discussion about changing the
greeter](https://discord.com/channels/743715854809563177/743715854809563180/896454935858786374)
(needs a registered account and being a member of [Pi
Talk](https://discord.com/invite/hNkdtbH) server)

The login screen (greeter) used by default in desktop images of
Raspbian, namely `pi-greeter` only allows logging in to the default
Raspberry Pi Desktop.

To be able to log in to another desktop environment (or any window
manager or custom session), this greeter should be replaced with one
that allows choosing a different session, like `lightdm-gtk-greeter`,
which is thankfully installed by default.

<div id="sudoedit">

For security and convenience reasons, the next commands [use `sudoedit` in place of running
`sudo <editor>`](https://old.reddit.com/r/linux/comments/osah05/) (e.g. `sudo nano`). This is merely a personal preference, you\'re free to use whatever fits you best.

If you choose to use `sudoedit` make sure [the environment variable
`EDITOR` is
set](https://askubuntu.com/questions/432524/how-do-i-find-and-set-my-editor-environment-variable)
to your favorite text editor, if you haven\'t previously done so.

</div>

Edit lightdm\'s main configuration file :

``` shell
sudoedit /etc/lightdm/lightdm.conf
```

Find the following line, below in the `[Seat:*]` section (line 108 for
me) :

``` ini
greeter-session=pi-greeter
```

Add a `#` at the beginning to turn it into a comment, effectively
disabling it :

``` ini
#greeter-session=pi-greeter
```

Save the file and exit (in `nano`, that\'s `Ctrl+X`, then `Y`, then
`Enter`)

This will remove Raspbian\'s hardcoded greeter choice, allowing us to
select another greeter through the [Debian alternatives
system](https://wiki.debian.org/DebianAlternatives).

To select another greeter in this way, run :

``` shell
sudo update-alternatives --config lightdm-greeter
```

This will display a list of greeters and prompt you to make a choice :

```
    There are 2 choices for the alternative lightdm-greeter (providing /usr/share/xgreeters/lightdm-greeter.desktop).
      Selection    Path                                              Priority   Status
    ------------------------------------------------------------
    * 0            /usr/share/xgreeters/pi-greeter.desktop            70        auto mode
      1            /usr/share/xgreeters/lightdm-gtk-greeter.desktop   60        manual mode
      2            /usr/share/xgreeters/pi-greeter.desktop            70        manual mode

    Press <enter> to keep the current choice[*], or type selection number: 
```

We want to use `lightdm-gtk-greeter`, so we type in `1` then press
`Enter`.

The following message will confirm the operation ran successfully :

    update-alternatives: using /usr/share/xgreeters/lightdm-gtk-greeter.desktop to provide /usr/share/xgreeters/lightdm-greeter.desktop (lightdm-greeter) in manual mode

To apply the change, reboot or restart lightdm by running
`sudo service lightdm restart`. (If you\'re automatically logged in, as
per the default in Raspbian, make sure to log out through the Shutdown
menu to display the greeter)

The new greeter will have a top panel, with a menu to select a desktop
session along other icons in top-right corner; it will be symbolised
either by your desktop environment\'s logo, or by an icon representing a
page and a wrench.

![](/raspbian/session_menu_gnome.jpg)
![](/raspbian/session_menu_other.jpg)

### Logging in to the Raspberry Pi Desktop from the new greeter

Selecting \"Default Xsession\" in the session selection menu seems to be
using the Raspberry Pi Desktop as a default. This might be since
Bullseye.

If it doesn\'t, or if that entry isn\'t present, the Raspberry Pi
Desktop can be added to the menu by creating a desktop entry for it in
the relevant folder.

(see [note about sudoedit](#sudoedit) above)

``` shell
sudoedit /usr/share/xsessions/LXDE-pi.desktop
```

Add the following contents inside the file :

``` ini
[Desktop Entry]
Name=Raspberry Pi Desktop
Exec=startlxde-pi
Type=Application
```

Save and exit, then log out to see the change.

### Network configuration on other desktops

After logging in to another desktop environment (DE), you might notice
that, although you\'re connected to a Wi-Fi or wired network and you can
reach websites from a web browser, the network configuration feature
provided by that desktop environment will report that it cannot
configure either network.

![](/raspbian/broken_network_config_cinnamon.png)
![](/raspbian/broken_network_config_gnome_panel.png)
![](/raspbian/broken_network_config_gnome_settings.png)

You might also face this problem the other way around : other DEs can
configure the network while the Raspberry Pi Desktop can\'t. (Although
it knows it\'s connected to a network, and which, contrary to other DEs
in this situation)

This is because most desktop environments rely on NetworkManager for
configuring the network, while the Raspberry Pi Desktop (RPD) uses
dhcpcd by default.

Only one of the two can work at the same time. When you install another
DE (that uses NetworkManager) alongside the default Raspberry Pi
Desktop, both NetworkManager and dhcpcd are installed and started during
boot. Whichever happens to start first seems to get control over the
network interfaces.

To be able to configure networking across all DEs, first disable dhcpcd,
to ensure NetworkManager can configure the network.

Note that [**you will have to
reconfigure and reconnect to your network**]{.underline} afterwards,
including re-entering your Wi-Fi network name and password, and you may
not be able to do it outside of a graphical desktop **in the same ways
as in default Raspbian** (e.g. `wpa_supplicant.conf`). **Do not
proceed** if you do not have **physical access** to the Pi.

``` shell
sudo systemctl disable dhcpcd.service
```

The following message will confirm the operation ran successfully :

    Removed /etc/systemd/system/multi-user.target.wants/dhcpcd.service.

Then install a (cross-desktop) configuration applet for NetworkManager :

``` shell
sudo apt install network-manager-gnome
```

Reboot to apply changes. Then log in to the Raspberry Pi Desktop.

The rightmost network configuration applet in the top panel should not
display anything useful when clicked: it is the RPD\'s default, which
uses dhcpcd.

![](/raspbian/broken_network_config_lxde-pi.gif)

You should see another network applet on the left of other status
applets : it is the applet for NetworkManager. You should now use this
applet for configuring your network.

![](/raspbian/nm-applet_rpd.png)

You can remove the default RPD applet by right-clicking it and selecting
`Remove ... from Panel`

In other DEs, you should be able to use the default network
configuration feature, or the NetworkManager applet.

\<WRAP center round tip 60%\> If you face issues after disabling dhcpcd,
you can enable it again by running :

``` shell
sudo systemctl enable dhcpcd.service
```

You should then also disable NetworkManager :

``` shell
sudo systemctl disable NetworkManager.service
```

*You will lose the ability to configure networking on DEs other than the
RPD.*

Reboot afterwards. \</WRAP\>

## Disabling automatic log in

FIXME done through the graphical raspberry pi configuration utility

automatic log in uses the last selected session(?)

## Pre-installed SD card

I have bought a 32GB SD card from the Raspberry Pi Foundation (at the
[Raspberry Pi Store](https://www.raspberrypi.com/raspberry-pi-store/))
that said it has \"Raspberry Pi OS\" on it preinstalled. It actually
contained NOOBS, itself used to install Raspbian (which seems to be the
general case for pre-installed SD cards sold by the Raspberry Pi
Foundation; questionable decision =)).

As I was starting to run out of space, I quickly noticed that the root
file system (containing the OS and personal files) was only 12GB out of
32GB.

I did not find any option to expand the filesystem in the provided
graphical configuration tools. The relevant setting in raspi-config only
returned the message :

    Your partition layout is not currently supported by this
    tool. You are probably using NOOBS, in which case you 
    root filesystem is already expanded anyway.

It isn\'t =)

[This Stack Exchange
question](https://raspberrypi.stackexchange.com/q/107499) describes the
same problem, but caused by the contents of a smaller card being copied
to a bigger one. If you\'re facing the same problem as with my SD card,
you can try the solutions listed. Although you might also just backup
any data and install a fresh Raspbian image on to the SD card.

I just hope this isn\'t a recurring problem with pre-installed SD cards
from the Raspberry Pi Foundation, because they are targeted towards
beginners who might end up thinking they were scammed :-/
