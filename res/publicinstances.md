---
eleventyExcludeFromCollections: true
---

# List of public instances of some self-hostable services

-   Bitwarden
	- [[https://vault.bitwarden.com/#/|The official public instance, of course.]]
    - [[https://vault.tedomum.net/#/|https://vault.tedomum.net]]
- Mattermost
    - [[https://tchat.colibris-outilslibres.org/|https://tchat.colibris-outilslibres.org]]
    - [[https://team.picasoft.net/|https://team.picasoft.net/]]
