---
title: "Logiciels de bureau"
parent: Pages en français
---

# Les logiciels de bureau que j\'utilise (et recommande)

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Fonctionnalité        Nom      Commentaire                                                                                                                                      Compatibilité                            Liens
  --------------------- -------- ------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------------- ---------------------------------------------------------------------
  Navigateur Internet   Falkon   Navigateur basé sur Chromium (Google) avec une interface simpliste mais toutes les options nécessaires, sans publicités ou fonctions inutiles.   Linux (Qt), Windows (ancienne version)   [Site officiel & installeurs Windows](https://www.falkon.org/),\
                                                                                                                                                                                                                           [Afficher dans votre logithèque](appstream://org.kde.falkon)\
                                                                                                                                                                                                                           [Installer via APT](apt://falkon)\
                                                                                                                                                                                                                           [Page sur Flathub](https://flathub.org/apps/details/org.kde.falkon)

                                                                                                                                                                                                                           

                                                                                                                                                                                                                           
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
