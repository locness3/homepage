---
title: Zorin OS Desktop on a Raspberry Pi 4
parent: Linux
---

# \"Zorin OS 16 Beta\" on the Raspberry Pi 4 (deliberate quotes)

TODO : display configuration

All of Zorin OS\'s components are available through [this
PPA](https://launchpad.net/~zorinos/+archive/ubuntu/stable). This
includes the latest things from Zorin OS 16.

The team said [they will make an ARM
version](https://forum.zorin.com/t/making-an-arm-version/124/8) of Zorin
OS 16 for the Raspberry Pi 4, but if you can\'t just wait, you can just
manually install the things from the PPA, on Ubuntu Server 20.04 for the
Pi. Here are some instructions :p

First of all, download Ubuntu Server 20.04 from [the official
website](https://ubuntu.com/raspberry-pi), and flash it to your SD card
with [USBImager](https://bztsrc.gitlab.io/usbimager) (or Etcher if you
like bloat).

Boot up and login with username and password `ubuntu`. Change the
password.

Then connect to an Ethernet network if you can. To connect to a Wi-Fi
network, use Netplan and follow [these](https://netplan.io/faq/)
[pages](https://netplan.io/examples/#using-dhcp-and-static-addressing)
([bis](https://netplan.io/examples/#connecting-to-a-wpa-personal-wireless-network)).

Then install gdm3, gnome-shell, and gnome-session.


    sudo apt update
    sudo apt install gdm3 gnome-shell gnome-session

Then add the Zorin PPA.

    sudo add-apt-repository ppa:zorinos/stable

Press Enter when prompted to.

Finally, install the Zorin Desktop.

    sudo apt update
    sudo apt install zorin-os-desktop

If you get a message about dependencies that can\'t be installed :

-   Disable the Zorin PPA:
    *

sudo nano /etc/apt/sources.list.d/zorinos-ubuntu-stable-focal.list

    * Add a ''#''  to the first line to comment it, and save the file.
    * Run ''sudo apt update''  and install the needed packages.
    * Re-enable the Zorin PPA by uncommenting the line in the same file.
    * Try to install the Zorin Desktop again (after an apt update)

Have fun!
