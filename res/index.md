---
title: Resources
parent: ""
---

# Ressources

## Bookmarks
I have many web bookmarks in my Shaarlis and other places; I will organize them in a set of pages merged with the below documentation and probably some more knowledge.

## Documentation

Here are some informative pages I've previously written.

These have not been updated in some time and many are unfinished.

They originally were published with DokuWiki, but I have converted them to Markdown with [pandoc] and now they're part of this static website generated with [Eleventy].

- [Awesome FOSS applications for Android](awesome-android-foss) (unfinished)
- [Linux](linux)
	+ [Locking down desktop environments](linux/desktop-lockdown)
	+ [About Raspbian](linux/raspbian) (one of my best pages)
	+ [Install the Zorin Desktop on a Raspberry Pi](zorinraspi) (can be improved)
- [About Matrix homeservers](matrix)
- [nginx notes](nginx) (can be clarified)
- [public instances of various services](publicinstances) 
- [UI design - Free stuff from Refactoring UI](refactoringui)
- [French language pages]
	+ [NewPipe tutorial](fr/newpipe) (unfinished)
	+ [Desktop software](logiciels/desktop) (unfinished)
