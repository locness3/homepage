---
title: LESS
parent: Resources
---

# LESS

In LESS, `@media [only] screen and ...` translates to
`@media [only] screen and screen and ...` =\> use straight up
`@media ...`
