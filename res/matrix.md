---
title: About Matrix homeservers
---

# Matrix homeservers

## Why use a \"custom\" homeserver, other than matrix.org?

Matrix has been thought as a decentralized network. Which means, in
theory, there is no central server. You can choose a homeserver from a
number of servers, managed by different people, that\'s in control of
your data.

Sadly, the Element Matrix client, which is the most popular one, and the
one with the most reach, by default uses the central matrix.org
homeserver. The two other offered options are signing up for a
\"premium\" server managed by Element Matrix Services, or using a
\"custom\" server - which is even considered an \"advanced\" option on
some versions.

See the problem? You have a decentralized network, but the most popular
client only promotes one centralized server and company as the default
option. Using another homeserver is an \"advanced\" option - which means
newcomers won\'t use it.

I want to say this is not designed with the intention of getting control
over the conversations of Matrix newcomers, tricked into thinking they
use a \"decentralized\" service, while they\'re actually not, but I
sincerely doubt it. I hope it\'s not, but I\'m not sure about it. The
Matrix Foundation doesn\'t even bother to maintain an official
homeserver list.

I think a good example of well-done decentralization is PeerTube. When
you go to the website and go to try it, you instantly get presented with
a list of instances (servers) to choose from. It would be good if Matrix
did the same thing, but I don\'t know what and whose interests are
behind it.

If it doesn\'t come from them, it should come from us. If you\'re using
matrix.org or plan to use Matrix, please sign up for a homeserver other
than matrix.org. I planned to do a list of homeservers here, but never found motivation to do it. I'll add links to homeserver lists. FIXME
