---
title: Tutoriel NewPipe
parent: "Pages en français"
---
# Ébauche : Regarder YouTube plus sainement et librement avec NewPipe

\- Application pour Android.

\- Client pour YouTube et autres sites de contenu audiovisuel.

\- Vie privée.

## Fonctionnement de NewPipe pour YouTube

\- Alternative aux applications et sites web YouTube officiels : YouTube
est utilisé comme source de contenu

\- N\'est pas un service, n\'a pas de serveurs propres

\- Abonnements et playlists gérés localement - pas de connexion à un
compte YouTube ou tiers (mais import de données possible)

\- N\'utilise pas les API de YouTube, mais récupère le contenu à partir
du site internet

\- Ce dernier change fréquemvidéos

ouvant limiter l\'utilisation de l\'application jusqu\'à une mise à jour

\- Pas de recommendations personnalisées. Cela peut être frustrant au
début mais ça permet de perdre moins de temps et de rester maître de ce
que l\'on regarde.

\- Pas de publicités.

## Installation et mise en place

### Installation

\- non disponible sur Google Play, va à l\'encontre des règles de Google
et YouTube

\- Depuis le site officiel, télécharger l\'apk

\- Autoriser les sources inconnues

### Importation des abonnements

\- Utiliser la page Takeout

\- Extraire l\'archive Takeout dans le gestionnaire de fichier - cela
dépend des versions d\'android

\- importer le fichier dans NP

### Ajout des nouveautés à la page d\'accueil

## Fonctionnalités utiles

\- Écoute en arrière-plan

\- Mise en fenêtre flottante

\- Télechargement de vidéos

### Utilisation avec PeerTube

\- Combiner PeerTube et YouTube, passer progressivement de l\'un à
l\'autre.

\- Configurer des instances PeerTube.

\- Trouver et s\'abonner à des chaînes PeerTube
