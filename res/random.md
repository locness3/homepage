---
eleventyExcludeFromCollections: true
nonfree: true
---

# Illegible dump of random things that should be organized

dconf keyfiles :
empty arrays should be quotes?
single and double quotes work the same?

https://askubuntu.com/questions/298533/dconf-and-locks

adduser's default NAME_REGEX doesn't allow for dots in usernames
- contrary to what's suggested in cinnamon settings > users and groups

adduser and addgroup "are friendlier front ends to the low level tools like useradd, groupadd and usermod programs, by default choosing Debian policy  conformant  UID  and  GID  values, creating a home directory with skeletal configuration, running a custom script,  and  other  features." according to adduser(8)

policykit is used for restrictively allowing access to priviliged things, like setting date and time, creating users, accessing devices, shutting down... in the context of DEs

though it appears polkit and PolicyKit are not the same : https://wiki.debian.org/PolicyKit
although polkit (the newer one) can still use older rules from PolicyKit (the older one) in addition to newer javascript rules

further reading on polkit :
http://smcv.pseudorandom.co.uk/2015/why_polkit/
https://lwn.net/Articles/258592/
https://wiki.archlinux.org/title/Polkit

freedesktop malcontent, a parental controls thing
instaleld by default in debian
used by complying software to filter :
- installing apps with ratings (gnome-software?)
- preventing specific (flatpak only?) apps from running

my homepage stylesheet, used as reference for other software :
https://codeberg.org/locness3/homepage/src/branch/master/style.css

[thème white]

marge gauche + contenu + marge droite = 1334px arrondi à 1400px

icônes dans un sprite, recolorées avec gimp

changements dans design.less
- couleur du titre du site
- toujours afficher la sidebar et cacher le bouton d'ouverture quand > 1400px

changements de couleurs réalisés dans Template Style Settings

apt : if post-installation of package failed (e.g. sddm could not run addgroup because of errors in adduser.conf), can't apt install --reinstall or dpkg-reconfigure? have to remove and install again

GNOME Photos's limitations are documented : https://wiki.gnome.org/Design/Apps/Photos/Storage section Relevant Art


The GNOME Wiki's [[https://wiki.gnome.org/Attic|attic]] has some pretty interesting stuff :
- [[https://wiki.gnome.org/Attic/AboutGnome|About the GNOME project, history] GNOME core under LGPL -> build non-free apps for GNOME
- [[https://wiki.gnome.org/Attic/Anjal]]
- [[https://wiki.gnome.org/Attic/ApplicationUsabilityNetbook|Testing of GNOME Shell and apps on notebook display (2013), highlighting some unrelated problems along the way, concluding with "most apps work fine" when important ones don't]]
- [[https://wiki.gnome.org/WeatherApplet]] - DavydMadeley: "if anything I release uses OPML, several people have indicated an intention to fly to Australia and cause me pain ;-)" ?

https://answers.microsoft.com/en-us/windows/forum/all/mainstream-support-extended-support/8b20a762-80ee-4459-95c4-491666afc8aa - During the 'Mainstream' support period, the product will receive enhancements and security upgrades. 'Extended' support loses the enhancements..
https://docs.microsoft.com/en-us/lifecycle/products/windows-81 - windows 8.1 will receive security updates until Jan 10, 2023

https://github.com/pyca/bcrypt#alternatives - scrypt, argon2id

https://flameshot.org/docs/guide/wayland-help/#i-am-asked-to-share-my-screen-every-time Flameshot developers blaming and being angry at GNOME developers for altering an internal API

#debian:matrix.debian.social
me:
Is there a particular reason why there are no Electron apps packaged within Debian?

@benthetechguy:fairydust.space:
most of them tend to be proprietary

electron itself isn't packaged because it's a huge project that nobody's decided to maintain yet

chromium, which it's based on, almost lost all of its maintainers in 2021, and is still very short on them, so it's not something you can take for granted


to make clients reload a cached asset of your webpage, modify the link with a query parameter e.g. a fake version number https://stackoverflow.com/a/48929822

https://en.wikipedia.org/wiki/GNU_FreeFont - FreeSans is based on URW++ Nimbus Sans L, which is similar to Helvetica

"your android sdk is missing, out of date, or corrupted" https://stackoverflow.com/a/49654117

[YAD - afficher des boîtes de dialogues graphiques depuis des
scripts](https://doc.ubuntu-fr.org/yad_yet_another_dialog)

[CSS : scroll snap, counter style, rhythmic styles, mirror element,
conic gradients, motion path, display shape media
query](https://pavellaptev.medium.com/lesser-known-css-properties-in-gifs-966a143497ba)

[The Ultimate Oldschool PC Font
Pack](https://int10h.org/oldschool-pc-fonts/fontlist/)

[Interpreting URLs with
JavaScript](https://codetheweb.blog/2019/01/21/javascript-url-parsing/)

It\'s hard to distinguish articles within Feeder\'s compact list since
Material You -\> export OPML file and import it into Readrops -\>
doesn\'t work -\> import it into Read You -\> works -\> export it from
Read You -\> try to import it in Readrops -\> Android\'s stupid file
picker doesn\'t recognize it as a text file and won\'t let me pick it
-\> open it through Editor\'s file browser -\> save it under a different
name -\> import it in Readrops -\> works; i love android

<https://support.xilinx.com/s/question/0D52E00006mfjcgSAA/vfs-cannot-open-root-device-mmcblk0p2-or-unknownblock1792-error-30-kria-kv260?language=en_US>

Older versions of Apple\'s Xcode can be downloaded from [Apple\'s
Developer Website](https://developer.apple.com/download/all/?q=xcode).
Signing in with an Apple ID and agreeing to the Apple Developer
Agreement is required. An unofficial website offers a [chronological
list of all the available Xcode versions, with direct download
links](https://xcodereleases.com/) with the minimum required macOS
version and the provided SDKs.
