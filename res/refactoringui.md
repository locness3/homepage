---
title: UI design - Free stuff from Refactoring UI
nonfree: true
---

# Refactoring UI

Refactoring UI is the name of a formerly-active blog and a paid-for e-book
aiming to provide practical tips to non-designers to help them design
good-looking user interfaces.

While I don\'t personnally agree with all of their recommendations
anymore, it might be of good use to you if you\'re looking to design
appealing software. Not too sure about usability or accessibility
though.

Below is a list of all freebies I could find from it.

-   [7 practical tips for cheating at design](https://medium.com/refactoring-ui/7-practical-tips-for-cheating-at-design-40c736799886)
-   [Redesigning Laravel.io](https://medium.com/refactoring-ui/redesigning-laravel-io-c47ac495dff0) -
    taking a very bad-looking website and making it much ebtter with
    only practical tips - wish they\'d done more of this kind of
    articles!
-   [Building your color palette](https://www.refactoringui.com/previews/building-your-color-palette)
-   [Labels are a last resort](https://www.refactoringui.com/previews/labels-are-a-last-resort) - when and when not to label information
-   [Start with too much whitespace](https://refactoring-ui.nyc3.cdn.digitaloceanspaces.com/Refactoring%20UI%20-%20Start%20with%20too%20much%20white%20space.pdf)
-   Building a dashboard (video) - where did i save that link FIXME

You may [buy the e-book here](https://www.refactoringui.com).
